from django.contrib import auth
from django.utils import timezone
import datetime
from COT import settings


class SessionTimeoutMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            last_activity = request.session.get('last_activity')
            if last_activity:
                last_activity = datetime.datetime.strptime(last_activity, '%Y-%m-%d %H:%M:%S.%f%z')  # Adjusted format string to include timezone
                idle_duration = timezone.now() - last_activity
                if idle_duration.total_seconds() > settings.SESSION_COOKIE_AGE:
                    auth.logout(request)

        response = self.get_response(request)

        if request.user.is_authenticated:
            request.session['last_activity'] = str(timezone.now())  # Convert datetime to string

        return response
