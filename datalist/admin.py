from django.contrib import admin
from datalist.models import Technical, AdministrativeDocument

class AdministrativeDocumentInline(admin.TabularInline):
    model = AdministrativeDocument

@admin.register(Technical)
class TechnicalAdmin(admin.ModelAdmin):
    inlines = [AdministrativeDocumentInline]
    list_display = [
        'product_name',
        'factory_number',
        'inv_number',
        'quantity',
        'unit_measurement',
        'price',
        'storage_location',
        'fixed_person',
        'status']
    search_fields = [
        'product_name',
        'factory_number',
        'inv_number',
        'price',
        'storage_location',
        'status',
    ]

@admin.register(AdministrativeDocument)
class AdministrativeDocumentAdmin(admin.ModelAdmin):
    list_display = [
        'technical',
        'document']
    search_fields = [
        'technical__product_name',  # Assuming you want to search by related field as well
        'document',
    ]
