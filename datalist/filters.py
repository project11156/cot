import django_filters
from .models import Technical

class TechnicalFilter(django_filters.FilterSet):
    category = django_filters.ChoiceFilter(choices=Technical.CATEGORY, label="Category")
    status = django_filters.ChoiceFilter(choices=Technical.STATUS_CHOICES, label="Status")
    storage_location = django_filters.CharFilter(lookup_expr='icontains', label="Storage Location")
    responsible_person = django_filters.CharFilter(lookup_expr='icontains', label="responsible_person")
    fixed_person = django_filters.CharFilter(lookup_expr='icontains', label="fixed_person")
    price = django_filters.RangeFilter(label="Price Range")  # Allows filtering by price range

    class Meta:
        model = Technical
        fields = ['category', 'status', 'storage_location', 'responsible_person', 'fixed_person','price']
