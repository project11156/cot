from django import forms
from .models import Technical, AdministrativeDocument

class UploadFileForm(forms.Form):
    file = forms.FileField()

class TechnicalForm(forms.ModelForm):
    class Meta:
        model = Technical
        fields = [
            'product_name',
            'factory_number',
            'inv_number',
            'quantity',
            'unit_measurement',
            'price',
            'storage_location',
            'responsible_person',
            'fixed_person',
            'item_passport_number',
            'date_of_start',
            'term_of_use',
            'term_of_use_document',
            'category',
            'status',
            'terms',
            'image',
        ]

        labels = {
            'product_name': 'Найменування  об’єкта(ів)',
            'factory_number': 'Зав. №',
            'inv_number': 'Інв. (номенклатур.) №',
            'quantity': 'К-сть.',
            'unit_measurement': 'Од. виміру',
            'price': 'Первісна вартість (сума)',
            'storage_location': 'Місцезнаходження об’єкта(ів)',
            'responsible_person': 'МВО (Метеріально відповідальна особа)',
            'fixed_person': 'Закріплено за (особа)',
            'item_passport_number': '№ формуляра (паспорта)' ,
            'date_of_start': 'Дата початку експлуатації',
            'term_of_use': 'Встановлений строк служби об’єкта(ів) в роках',
            'term_of_use_document': 'Керівний документ по строкам служби об’єкта(ів)',
            'category': 'Категорія  об’єкта(ів)',
            'status': 'Статус  об’єкта(ів)',
            'terms': 'Примітка',
            'image': 'Світлина',
        }

        widgets = {
            'date_of_start': forms.DateInput(attrs={'type': 'date'}),
        }

class AdministrativeDocumentForm(forms.ModelForm):
    class Meta:
        model = AdministrativeDocument
        fields = ['document']

        labels = {
            'document': 'Документ',
        }
    def clean_document(self):
        document = self.cleaned_data.get('document')
        max_size = 20 * 1024 * 1024  # 20MB limit
        if document and document.size > max_size:
            raise forms.ValidationError("File size should not exceed 5MB.")
        return document

TechnicalFormSet = forms.inlineformset_factory(
    Technical,
    AdministrativeDocument,
    form=AdministrativeDocumentForm,
    extra=3,  # Number of extra forms (adjust as needed)
    can_delete=True,
)