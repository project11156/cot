# Generated by Django 4.2.3 on 2024-03-02 15:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datalist', '0015_documents_remove_technical_administrative_document_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='technical',
            name='docs',
        ),
        migrations.AddField(
            model_name='technical',
            name='docs',
            field=models.FileField(blank=True, null=True, upload_to='docs/'),
        ),
    ]
