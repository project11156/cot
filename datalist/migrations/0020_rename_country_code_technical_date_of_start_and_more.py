# Generated by Django 4.2.3 on 2024-04-17 08:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datalist', '0019_technical_country_code_technical_serial_number'),
    ]

    operations = [
        migrations.RenameField(
            model_name='technical',
            old_name='country_code',
            new_name='date_of_start',
        ),
        migrations.RenameField(
            model_name='technical',
            old_name='item_passport',
            new_name='factory_and_inv_numbers',
        ),
        migrations.RenameField(
            model_name='technical',
            old_name='serial_number',
            new_name='item_passport_number',
        ),
        migrations.RemoveField(
            model_name='technical',
            name='factory_and_inv_number',
        ),
        migrations.AddField(
            model_name='technical',
            name='factory_number',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='technical',
            name='fixed_person',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='technical',
            name='inv_number',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='technical',
            name='item_passport_number_and_date_of_start',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='technical',
            name='responsible_and_fixed_person',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
