from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import get_object_or_404
from datalist.forms import TechnicalForm, TechnicalFormSet
from django.urls import reverse
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.db.models import Q
import io
from django.shortcuts import render, redirect
from django.contrib import messages
import pandas as pd
from .filters import TechnicalFilter
from .models import Technical, User
from django.utils.timezone import now
from .forms import UploadFileForm


def user_login(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('/')  # Replace 'home' with your app's home URL
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

def user_logout(request):
    logout(request)
    return redirect('login')


@login_required
def datalist(request):
    all_data = Technical.objects.prefetch_related('docs').order_by('id').all()

    # Apply filters
    filterset = TechnicalFilter(request.GET, queryset=all_data)

    return render(request, 'datalist.html', {
        'filter': filterset,
        'data': filterset.qs,
        'params': request.GET.urlencode()
    })

@login_required
def search(request):
    query = request.GET.get('q')  # Get the search query from the request
    all_data = Technical.objects.all()  # Get all data first

    # Apply search filtering
    if query:
        all_data = all_data.filter(
            Q(user__username__icontains=query) |
            Q(product_name__icontains=query) |
            Q(inv_number__icontains=query) |
            Q(factory_number__icontains=query) |
            Q(quantity__icontains=query) |
            Q(unit_measurement__icontains=query) |
            Q(price__icontains=query) |
            Q(storage_location__icontains=query) |
            Q(responsible_person__icontains=query) |
            Q(fixed_person__icontains=query) |
            Q(date_of_start__icontains=query) |
            Q(item_passport_number__icontains=query) |
            Q(term_of_use__icontains=query) |
            Q(term_of_use_document__icontains=query) |
            Q(status__icontains=query) |
            Q(terms__icontains=query)
        )

    # Apply additional filters
    filterset = TechnicalFilter(request.GET, queryset=all_data)

    return render(request, 'search.html', {
        'results': filterset.qs,  # Apply both search and filters
        'query': query,
        'filter': filterset  # Pass the filter form to the template
    })

@login_required
def create_item(request):
    if request.method == 'POST':
        technical_form = TechnicalForm(request.POST, request.FILES)
        formset = TechnicalFormSet(request.POST, request.FILES)

        if technical_form.is_valid() and formset.is_valid():
            technical_instance = technical_form.save(commit=False)
            technical_instance.user = request.user
            technical_instance.save()

            instances = formset.save(commit=False)
            for instance in instances:
                instance.technical = technical_instance
                instance.save()

            return redirect('success_page')

    else:
        technical_form = TechnicalForm()
        formset = TechnicalFormSet()

    return render(request, 'create_item.html', {
        'technical_form': technical_form,
        'formset': formset,
    })

@login_required
def change_item(request, item_id):
    item = get_object_or_404(Technical, pk=item_id)

    if request.method == 'POST':
        technical_form = TechnicalForm(request.POST, request.FILES, instance=item)
        formset = TechnicalFormSet(request.POST, request.FILES, instance=item)

        if technical_form.is_valid() and formset.is_valid():
            technical_form.instance.user = request.user
            technical_form.save()
            formset.save()
            return redirect(reverse('success_page'))

    else:
        technical_form = TechnicalForm(instance=item)
        formset = TechnicalFormSet(instance=item)

    return render(request, 'change_item.html', {
        'technical_form': technical_form,
        'formset': formset
    })

@login_required
def success_page(request):
    return render(request, 'success.html')


@login_required
def export_excel(request):
    query = request.GET.get('q', '')

    # Retrieve filter parameters from the request
    all_data = Technical.objects.prefetch_related('docs').order_by('id').all()

    filterset = TechnicalFilter(request.GET, queryset=all_data)
    filtered_data = filterset.qs  # Apply filters

    # Define required fields
    required_fields = [
        'user__username',
        'product_name',
        'factory_number',
        'inv_number',
        'quantity',
        'unit_measurement',
        'price',
        'storage_location',
        'responsible_person',
        'fixed_person',
        'item_passport_number',
        'date_of_start',
        'term_of_use',
        'term_of_use_document',
        'category',
        'status',
        'terms'
    ]

    # Apply search filters if a query is present
    if query:
        technical_objects = Technical.objects.filter(
            Q(user__username__icontains=query) |
            Q(product_name__icontains=query) |
            Q(inv_number__icontains=query) |
            Q(factory_number__icontains=query) |
            Q(quantity__icontains=query) |
            Q(unit_measurement__icontains=query) |
            Q(price__icontains=query) |
            Q(storage_location__icontains=query) |
            Q(responsible_person__icontains=query) |
            Q(fixed_person__icontains=query) |
            Q(date_of_start__icontains=query) |
            Q(item_passport_number__icontains=query) |
            Q(term_of_use__icontains=query) |
            Q(term_of_use_document__icontains=query) |
            Q(status__icontains=query) |
            Q(terms__icontains=query)
        ).values(*required_fields)
    else:
        technical_objects = filtered_data.values(*required_fields)

    # Convert to DataFrame
    df = pd.DataFrame(list(technical_objects))

    # Create an in-memory output file
    output = io.BytesIO()

    # Write DataFrame to an Excel file
    with pd.ExcelWriter(output, engine='openpyxl') as writer:
        df.to_excel(writer, index=False, sheet_name='Technical Data')

    # Set response parameters
    response = HttpResponse(
        output.getvalue(),
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response['Content-Disposition'] = 'attachment; filename="technical_data.xlsx"'

    return response


def import_excel_to_postgresql(file):
    """Function to process the uploaded Excel file and save data to PostgreSQL"""
    df = pd.read_excel(file, engine='openpyxl')
    duplicate_messages = []  # List to store duplicate messages

    for _, row in df.iterrows():
        try:
            username = str(row.get('user__username', '')).strip()  # Using 'user' instead of 'username'

            user = User.objects.get(username=username)  # Find user by username

            factory_number = row.get('factory_number', '---')
            inv_number = row.get('inv_number', '---')

            # Check for existing records with the same factory_number or inv_number
            if Technical.objects.filter(factory_number=factory_number).exists() or \
               Technical.objects.filter(inv_number=inv_number).exists():
                message = f"Пропускання повторного запису для Заводський №: {factory_number}, Інв. (номенклатур.) №: {inv_number}"
                duplicate_messages.append(message)
                continue  # Skip this record

            technical = Technical(
                user=user,
                product_name=row['product_name'],
                factory_number=row.get('factory_number', '---'),
                inv_number=row.get('inv_number', '---'),
                quantity=int(row['quantity']),
                unit_measurement=row.get('unit_measurement', 'шт.'),
                price=float(row['price']),
                storage_location=row['storage_location'],
                responsible_person=row.get('responsible_person', '---'),
                fixed_person=row.get('fixed_person', '---'),
                item_passport_number=row.get('item_passport_number', '---'),
                date_of_start=pd.to_datetime(row['date_of_start'], errors='coerce').date() if pd.notna(row['date_of_start']) else now().date(),
                term_of_use=int(row['term_of_use']),
                term_of_use_document=row.get('term_of_use_document', '---'),
                category=row.get('category', '---'),
                status=row.get('status', '---'),
                terms=row.get('terms', '---')
            )
            technical.save()
        except Exception as e:
            duplicate_messages.append(f"Error importing row {row.to_dict()}: {e}")

    return duplicate_messages  # Return duplicate messages


def upload_file(request):
    """Handles file upload and triggers the import function"""
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES['file']
            duplicate_messages = import_excel_to_postgresql(file)  # Get duplicate messages

            if duplicate_messages:
                for msg in duplicate_messages:
                    messages.warning(request, msg)  # Store duplicate messages

            messages.success(request, "Файл успішно імпортовано!")
            return redirect('upload_page')  # Redirect to the upload page

    else:
        form = UploadFileForm()

    return render(request, 'upload.html', {'form': form})
